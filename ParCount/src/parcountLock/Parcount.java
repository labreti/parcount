/* Variante con i lock espliciti */
package parcountLock;

import java.util.concurrent.locks.ReentrantLock;

public class Parcount extends Thread {
	private static int N=1000;
	private static int x=0;
	private static final ReentrantLock l = new ReentrantLock();
	public static void main(String[] args) 
			throws InterruptedException {
		try { 
			N = Integer.valueOf(args[0]);
		} catch ( Exception e) {
			N = 1000;
		}
		Thread[] t = new Thread[N];
		int i;
		for ( i=N-1; i>=0; i-- ) { 
			t[i]=new Parcount(); 
			t[i].start();
		}
		for ( i=N-1; i>=0; i-- ) t[i].join();
		System.out.println(x);
	}
	public void run() { l.lock(); x++; l.unlock(); }
}
