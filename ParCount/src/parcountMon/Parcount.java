/* Variante con un blocco synchronized (aka monitor) */
package parcountMon;

public class Parcount extends Thread {
	private static int N=1000;
	private static int x=0;
	private static final Object monitor = new Object();
	
	public static void main(String[] args) 
			throws InterruptedException {
		Thread[] t = new Thread[N];
		int i;
		for ( i=N-1; i>=0; i-- ) { 
			t[i]=new Parcount(); 
			t[i].start();
		}
		for ( i=N-1; i>=0; i-- ) t[i].join();
		System.out.println(x);
	}
	public void run() { synchronized(monitor) { x++; } }
}
