/* Variante evitando gli "static", ma introducendo
 * una nuova classe "static nested" */
package parcountNoStatic;

import java.util.concurrent.locks.ReentrantLock;

public class Parcount {
	public static void main(String[] args) throws InterruptedException {
		int N=1000;
		final ReentrantLock l = new ReentrantLock();
		Thread T[] = new Thread[N];
		Integer x[]= new Integer[1];
		x[0]=0;
		int i;
		for ( i=N-1; i>=0; i-- ) { 
			T[i]=new Count(l,x); 
			T[i].start();
		}
		for ( i=N-1; i>=0; i-- ) T[i].join();
		System.out.println(x[0]);
	}
static class Count extends Thread {
	private ReentrantLock l;
	private Integer x[];
	public Count(ReentrantLock l, Integer x[]) { this.l=l; this.x=x; }
	public void run() { l.lock(); x[0]++; l.unlock(); }
	}
}
