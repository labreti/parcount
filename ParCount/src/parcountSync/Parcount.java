/* Variante con un metodo synchronized in
 * una classe "inner" */
package parcountSync;

public class Parcount extends Thread {
	private static int N=1000;
	private static Counter c;
	public static void main(String[] args) 
			throws InterruptedException {
		c=new Counter();
		Thread[] t = new Thread[N];
		int i;
		for ( i=N-1; i>=0; i-- ) { 
			t[i]=new Parcount(); 
			t[i].start();
		}
		for ( i=N-1; i>=0; i-- ) t[i].join();
		System.out.println(c);
	}
	public void run() { c.inc(); }
}

class Counter {
	private Integer x=0;
	public synchronized void inc() { x++; }
	public String toString() { return x.toString();}
}