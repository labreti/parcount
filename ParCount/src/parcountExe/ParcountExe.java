/* Variante con gli executors */
package parcountExe;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

public class ParcountExe implements Runnable {
    private static final int N=1000;
    private static int x=0;
	private static final ReentrantLock l = new ReentrantLock();

    public static void main(String[] args) throws InterruptedException {
        ArrayList<Callable<Object>> tasks = new ArrayList<>(N);
        for (int i = 0; i < N; i++)
            tasks.add(Executors.callable(new ParcountExe()));
        Executors.newFixedThreadPool(N).invokeAll(tasks);
        System.out.println(x);
        System.exit(0);
    }
	public void run() { l.lock(); x++; l.unlock(); }
}
